"""
Contains setup functions for test data, these are returned in a declarative.Bunch dictionary, with some annotation about the number of data sets
"""
from os import path

from IIRrational.testing import utilities
from IIRrational import testing
import numpy as np


data_folder = path.join(path.split(__file__)[0], 'data')

def HTTS_P_L(**kwargs):
    dset = testing.testcase2data(path.join(data_folder, 'HTTS_P_L.mat'))
    #from a particularly nice fit. Probably could be yet lower order though
    return utilities.generator_autofill(
        F_Hz               = dset.F_Hz,
        data               = dset.data,
        SNR                = dset.SNR,
        F_nyquist_Hz       = None,
        **kwargs
    )

def HTTS_Y_L(**kwargs):
    dset = testing.testcase2data(path.join(data_folder, 'HTTS_Y_L.mat'))
    #from a particularly nice fit. Probably could be yet lower order though
    return utilities.generator_autofill(
        F_Hz               = dset.F_Hz,
        data               = dset.data,
        SNR                = dset.SNR,
        F_nyquist_Hz       = None,
        **kwargs
    )


def PUM_long(**kwargs):
    F_Hz, mag, phase = np.loadtxt(path.join(data_folder, '2019-02-08_H1SUSETMX_PUM_Driver_ACQOFF_UL_tf.txt')).T
    xfer = mag * np.exp(1j * np.pi * phase / 180)

    #from a particularly nice fit. Probably could be yet lower order though
    return utilities.generator_autofill(
        F_Hz               = F_Hz,
        data               = xfer,
        SNR                = 1,
        F_nyquist_Hz       = None,
        **kwargs
    )


datasets = dict(
    HTTS_Y_L = utilities.make_description(
        generator = HTTS_Y_L,
        instances = 1,
    ),
    HTTS_P_L = utilities.make_description(
        generator = HTTS_P_L,
        instances = 1,
    ),
    PUM_long = utilities.make_description(
        generator = PUM_long,
        instances = 1,
    ),
)

