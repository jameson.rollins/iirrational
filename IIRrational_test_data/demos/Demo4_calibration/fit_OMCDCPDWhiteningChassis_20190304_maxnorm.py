#!/opt/local/bin/python

from collections import namedtuple
from matplotlib import pyplot as plt
import scipy.signal as sig
import numpy as np
#import IIRrational
import IIRrational.v2
import IIRrational.plots
import importlib
from IIRrational.testing import IIRrational_data
# from numpy import * (so you don't have to have to type np.pi or np.angle)
# from numpy import pi, angle, abs
import os
import sys

iPoles = [10, 10, 50, 7500, 19000, 19000]
iZeros = [1, 1, 500, 100000, 100000]

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 3,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9})

#This gets the root path of the SVN repo
path = os.path.abspath(__file__)
dirs = path.split('Common')
SVNROOT = dirs[0]

sys.path.insert(0, SVNROOT+'Common/pyDARM')
sys.path.insert(1, SVNROOT+'Runs/O3/H1/params')
dataDir=SVNROOT + 'Common/Electronics/H1/Data/OMCWhiteningChassis/2019-03-04/'
dataDir='./'
resultsDir=SVNROOT + 'Common/Electronics/H1/Results/OMCWhiteningChassis/'
resultsDir = './'

measDate = '2019-03-04'
fileTag = measDate+'_H1OMC_WhiteningChassisTFs_'
titleTag = measDate+' H1 OMC Whitening Chassis'
filterName = ['FM1FM2FM3','FM1FM2','FM1','noFM']
pdName = ['PDA','PDB']


for iPD in range(1):
    allResidual = []
    for iFM in range(1):
        '''
        iCh = str(iCoil+1)
        if iCoil == 0:
            fileName = dataDir + 'TFSR785_03-02-2019_170349.txt'
        if iCoil == 1:
            fileName = dataDir + 'TFSR785_03-02-2019_170958.txt'
        if iCoil == 2:
            fileName = dataDir + 'TFSR785_03-02-2019_171532.txt'
        if iCoil == 3:
            fileName = dataDir + 'TFSR785_03-02-2019_172119.txt'
        '''
        magFile = dataDir + 'srs0020.asc'
        phaFile = dataDir + 'srs0021.asc'
        plotFile = resultsDir + fileTag + '%s_%s_maxnorm.pdf' % (pdName[iPD], filterName[iFM])
        #contriPlotFile = resultsDir + '2019-02-03_H1_UIM_State1_residual_contribution-fit-IIRrational.pdf'
        print('Loading ')
        print(magFile)
        print(phaFile)
        
        freq_array = []
        dbmag_array = []
        pha_array = []

        f = open(magFile,'r')
        for line in f.readlines()[17:]:
            freq_array.append(np.float(line.split()[0]))
            dbmag_array.append(np.float(line.split()[1]))
        f.close()

        f = open(phaFile,'r')
        for line in f.readlines()[17:]:
            pha_array.append(np.float(line.split()[1]))
        f.close()

        mag = 10**(np.array(dbmag_array)/20)
        pha = np.array(pha_array) 
        freq = np.array(freq_array)
        tf_meas = mag * np.exp(1j*np.pi/180.0*pha)

        selectVector = (freq > 1) & (freq < 100000) # straight frequency cut
        snrWeight = abs(100 / (1 + 1j*freq/50)**1.2) # Lee's magic weighting
        delayMax=1e-4 # sec

        fit=IIRrational.v2.data2filter(
            data=tf_meas,
            F_Hz=freq,
            F_nyquist_Hz=None,
            delay_s_max=delayMax, # sec
            select = selectVector,
            SNR = snrWeight,
            mode='fit', #takes suggested poles/zeros only, doesn't try to get fancy with high order fits.
            poles = -1 * np.array(iPoles), # -1 to get it into "f" domain
            zeros = -1 * np.array(iZeros), # -1 to get it into "f" domain
            #feature in new version
            h_infinity = 1/np.linspace(2, 100, len(freq[selectVector])),
            # internal feature of IIRrational, preserves realness/complexness!
            coding_map = IIRrational.fitters_ZPK.codings_s.coding_maps.nlFBW,
        )

            # to look up in the future:
            # zeros_overlay / poles overlay -- to ignore "known" features like AC coupling

        fit.investigate_fit_plot(fname = 'OMCDCPD_maxnorm.png')
        print(' ')
        print(pdName[iPD] + ', Filters: ' + filterName[iFM] +' Results:')
        print(fit.as_ZPKrep().zeros.fullplane) # in Hz (but in the "f" domain, so with -1)
        print(fit.as_ZPKrep().poles.fullplane) # in Hz
        print(fit.as_ZPKrep().gain)
        #print(fit.as_foton_str_ZPKsf()) # print as a foton string
        print('    --    ')

        fitZeros = -1 * np.array(fit.as_ZPKrep().zeros.fullplane) # Convert to "n" plane (positive Hz)
        fitPoles = -1 * np.array(fit.as_ZPKrep().poles.fullplane) # Convert to "n" plane (positive Hz)
        fitGain  = np.array(fit.as_ZPKrep().gain)
        # tf_fit = fit.fitter.xfer_fit # on freq vector you gave for fit (may be a bug in there...)
        tf_fit = fit.fitter.xfer_eval(freq) # on your own frequency vector

        fotonString = fit.as_foton_str_ZPKsf()
        
        residual = tf_meas / tf_fit
        allResidual.append(residual)
        
        fig = plt.figure()
        s1 = fig.add_subplot(221)
        s2 = fig.add_subplot(223)
        s3 = fig.add_subplot(222)
        s4 = fig.add_subplot(224)

        s1.loglog(freq,abs(tf_meas),'.',label='Data')
        s1.loglog(freq,abs(tf_fit),'--',label='Fit')
        s1.set_ylabel('Magnitude (dimensionless)')
        s1.grid(which='minor', ls='--')
        #s1.set_ylim([1e-3, 1e1])
        s1.set_title(titleTag)
        s1.legend(ncol=2)

        s2.semilogx(freq,180.0/np.pi*np.angle(tf_meas),'.',label='Data')
        s2.semilogx(freq,180.0/np.pi*np.angle(tf_fit),'--',label='Fit')
        s2.set_xlabel('Frequency (Hz)')
        s2.set_ylabel('Phase (deg)')
        s2.grid(which='minor', ls='--')
        #s2.set_ylim(-90,90)
        s2.set_yticks(range(-180,180+15,30))
        s2.set_title('z:p:k = {}:{}:{}'.format(np.array2string(fitZeros,precision=3),np.array2string(fitPoles,precision=3),np.array2string(fitGain,precision=3)))

        s3.semilogx(freq,abs(residual),'.')
        s3.set_title(pdName[iPD] + ', Filters: ' + filterName[iFM])
        s3.set_ylim(0.99,1.01)
        s3.set_yticks(np.arange(0.99,1.01,0.002))
        s3.set_xlim(1,1e5)
        s3.grid(which='minor', ls='--')
        
        s4.semilogx(freq,180/np.pi*np.angle(residual),'.')
        s4.set_ylim(-0.5,0.5)
        s4.set_yticks(np.arange(-0.5,0.5,0.1))
        s4.set_xlim(1,1e5)
        s4.set_xlabel('Frequency (Hz)')
        s4.grid(which='minor', ls='--')
        plt.savefig(plotFile, bbox_inches='tight', pad_inches=0.2)
'''
#load the model parameters
mod = importlib.import_module('modelparams_H1_20190118')
func = getattr(mod, 'modelPars')
pars = func()
ff = freq

#now create the DARM model 
[DDNom, CCNom, A, G, CLG, RRNom, sensProd, actProd] = computeDARM(pars, ff)
AUNom = actProd.UIM
APNom = actProd.PUM
ATNom = actProd.TST

avgResidual = (allResidual[0] + allResidual[1] + allResidual[2] + allResidual[3])/4.0

invC = 1.0/ CCNom  #/RRNom
AT = DDNom * ATNom #/RRNom
AP = DDNom * APNom #/RRNom
AU = DDNom * AUNom #/RRNom
AUerr = DDNom * AUNom * avgResidual #/RRNom 

Response = invC + AT + AU + AP
ResponseErr = invC + AT + AUerr + AP
ResponseRes = Response/ResponseErr

h = plt.figure(figsize=(8,8))
f1 = h.add_subplot(211)
f2 = h.add_subplot(212)
f1.loglog(ff, np.abs(ResponseRes-1), label =r'H1 UIM contribution residual')
f1.legend(loc=1)
f1.set_xlim(1,1e3)
f1.set_ylabel('Mag')
f1.set_title(titleTag + ', State: '+stateName[iState])
f1.grid(which='minor', ls='--')
f1.set_ylim(1e-10,0.1)

f2.semilogx(ff, 180/np.pi*np.angle(ResponseRes), label =r'H1 UIM contribution residual')
f2.set_xlabel('Frequency (Hz)')
f2.set_ylabel('Phase (deg)')
f2.set_xlim(1,1e3)
f2.set_ylim(-0.2,0.2)
f2.legend(loc=1)
f2.grid(which='minor', ls='--')

plt.savefig(contriPlotFile, bbox_inches='tight', pad_inches=0.2)
'''
