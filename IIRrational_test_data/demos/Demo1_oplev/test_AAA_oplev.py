# -*- coding: utf-8 -*-
"""
"""
from __future__ import division, print_function, unicode_literals, absolute_import

import pytest
import numpy as np
from os import path
from declarative.bunch import DeepBunch

from IIRrational.pytest import (  # noqa: F401
    tpath_join, plot, pprint, tpath, tpath_preclear
)
import IIRrational
from IIRrational.v2.__main__ import IIRrationalV2fit
from IIRrational.v2.SNR_estimate import SNR_estimate

from IIRrational.utilities.mpl import mplfigB


def test_AAA_mod(tpath_join, tpath_preclear, pprint):
    from IIRrational.AAA import tfAAA
    dname =  'OplevPlant_invs.mat'
    dfile = path.join(path.split(__file__)[0], dname)
    data = DeepBunch(IIRrational.load(dfile))

    F_Hz = data.ap.ex.ff
    TF1 = data.ap.ex.plant
    w = SNR_estimate(F_Hz, TF1)
    w = np.minimum(w, 10)

    select = F_Hz < 10
    F_Hz = F_Hz[select]
    TF1 = TF1[select]
    w = w[select]

    fitB = tfAAA(
        F_Hz = F_Hz,
        xfer = TF1,
        w = w,
        w_minmax = np.minimum(np.maximum(w - 3, 0), 10),
        degree_max = 20,
        nconv = 2,
        nrel = 10,
        rtype = 'log',
        #supports = (1e-2, 1e-1, 4.2e-1, 5.5e-1, 1.5, 2.8, 1, 5e-1, 2),
    )

    pprint(fitB.N_w_s_fit)
    order = 20
    axB = mplfigB(Nrows = 3)
    axB.ax0.loglog((fitB.sF_Hz_drop / 1j).real, abs(fitB.res))
    axB.ax0.loglog((fitB.sF_Hz_drop / 1j).real, abs(fitB.res_max_asq**0.5))
    axB.ax1.loglog((fitB.sF_Hz_drop / 1j).real, abs(fitB.w_drop))
    axB.ax2.semilogx((fitB.sF_Hz_drop / 1j).real, fitB.res.real)
    axB.ax2.semilogx((fitB.sF_Hz_drop / 1j).real, fitB.res.imag)
    #axB.ax1.semilogx(F_Hz, np.angle(TF1, deg = True))
    #axB.ax1.semilogx(F_Hz, np.angle(TF2, deg = True))
    for z in fitB.zvals[:order]:
        axB.ax0.axvline(z/1j)
    axB.save(tpath_join('debug'))

    axB = mplfigB(Nrows = 2)
    axB.ax0.loglog(fitB.sF_Hz_drop/1j, abs(fitB.xfer_drop))
    axB.ax0.semilogy(fitB.sF_Hz_drop/1j, abs(fitB.fit_drop))
    axB.ax1.semilogx(fitB.sF_Hz_drop/1j, np.angle(fitB.xfer_drop, deg = True))
    axB.ax1.semilogx(fitB.sF_Hz_drop/1j, np.angle(fitB.fit_drop, deg = True))
    for z in fitB.zvals[:order]:
        axB.ax0.axvline(z/1j)
    axB.save(tpath_join('debug2'))

    order = 20
    TF2 = fitB.interp(F_Hz, order = order)
    axB = mplfigB(Nrows = 2)
    axB.ax0.loglog(F_Hz, abs(TF1))
    axB.ax0.semilogy(F_Hz, abs(TF2))
    axB.ax1.semilogx(F_Hz, np.angle(TF1, deg = True))
    axB.ax1.semilogx(F_Hz, np.angle(TF2, deg = True))
    for z in fitB.zvals[:order]:
        axB.ax0.axvline(z/1j)
    axB.save(tpath_join('test'))
    return


def test_AAA_oplev(tpath_join, tpath_preclear, pprint):
    dname =  'OplevPlant_invs.mat'
    dfile = path.join(path.split(__file__)[0], dname)
    data = DeepBunch(IIRrational.load(dfile))

    F_Hz = data.ap.ex.ff
    TF1 = 1/data.ap.ex.plant
    w = SNR_estimate(F_Hz, TF1)

    select = F_Hz < 10
    F_Hz = F_Hz[select]
    TF1 = TF1[select]
    w = 1 # w[select] + 10
    sF_Hz = 1j*F_Hz


    fvals = []
    zvals = []
    wvals = []
    for Fidx in [0, 55, 60, 70, 180, 330, 100, 200]:
        fvals.append(TF1[Fidx])
        zvals.append(sF_Hz[Fidx])
        #fvals.append(TF1[Fidx].conjugate())
        #zvals.append(-sF_Hz[Fidx])

    Vn = []
    Vd = []

    Widx = 0
    bad_Fidx = []
    while Widx < len(fvals):
        z = zvals[Widx]
        f = fvals[Widx]
        if z == 0:
            assert(f.imag == 0)
            bary_D = (sF_Hz - z)
            select_bad = abs(bary_D) < 1e-13
            bad_Fidx.extend(np.argwhere(select_bad)[:, 0])
            bary_D[select_bad] = float('NaN')
            bary_D = 1/bary_D
            with np.errstate(divide='ignore', invalid='ignore'):
                Vn.append(f * bary_D)
                Vd.append(bary_D)
        else:
            bary_D = (sF_Hz - z)
            select_bad = abs(bary_D) < 1e-13
            bad_Fidx.extend(np.argwhere(select_bad)[:, 0])
            bary_D[select_bad] = float('NaN')

            bary_D2 = (sF_Hz - z.conjugate())
            select_bad = abs(bary_D2) < 1e-13
            bad_Fidx.extend(np.argwhere(select_bad)[:, 0])
            bary_D2[select_bad] = float('NaN')

            #with np.errstate(divide='ignore', invalid='ignore'):
            #    Vn.append(f / bary_D)
            #    Vd.append(1/bary_D)
            #    Vn.append(f.conjugate() / bary_D2)
            #    Vd.append(1/bary_D2)

            with np.errstate(divide='ignore', invalid='ignore'):
                Vn.append(f / bary_D + f.conjugate() / bary_D2)
                Vd.append(1/bary_D + 1/bary_D2)
                Vn.append(-1j*(f / bary_D - f.conjugate() / bary_D2))
                Vd.append(-1j*(1/bary_D - 1/bary_D2))
        Widx += 1
    Vn = np.asarray(Vn).T
    Vd = np.asarray(Vd).T
    pprint(bad_Fidx)
    Vd[bad_Fidx, :] = 0
    Vn[bad_Fidx, :] = 0

    N = D = 1
    for _i in range(3):
        Hd1 = Vd * TF1.reshape(-1, 1)
        Hn1 = Vn
        Hd2 = Vd
        Hn2 = Vn * (1 / TF1).reshape(-1, 1)
        Na = np.mean(abs(N)**2)**0.5/5
        Da = np.mean(abs(D)**2)**0.5/5
        Hs1 = (Hd1-Hn1) * (w / (abs(N)) + Na).reshape(-1, 1)
        Hs2 = (Hd2-Hn2) * (w / (abs(D)) + Da).reshape(-1, 1)

        Hs1[bad_Fidx, :] = 0
        Hs2[bad_Fidx, :] = 0

        Hblock = []
        Hblock.append([Hs1.real])
        Hblock.append([Hs1.imag])
        Hblock.append([Hs2.real])
        Hblock.append([Hs2.imag])
        SX1 = np.block(Hblock)
        u, s, v = np.linalg.svd(SX1)
        pprint(s)
        pprint(v)
        Ws = v[-1, :].conjugate()
        pprint(Ws)
        #Ws = np.array(wvals)
        pprint("Ws", Ws)
        #Ws = [1 + 1j, 1j - 1j]

        N = Vn @ Ws
        D = Vd @ Ws

        TF2 = N / D

    axB = mplfigB(Nrows = 2)
    axB.ax0.loglog(F_Hz, abs(TF1))
    axB.ax0.semilogy(F_Hz, abs(TF2))
    axB.ax1.semilogx(F_Hz, np.angle(TF1, deg = True))
    axB.ax1.semilogx(F_Hz, np.angle(TF2, deg = True))
    for z in zvals:
        axB.ax0.axvline(z/1j)
    axB.save(tpath_join('test'))
    return


