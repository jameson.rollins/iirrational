IIRrational transfer function fitter
====================================

Version 2.

Features
--------

 * Fits in S-domain
 * Command line interface!
 * Initial guess optional
 * Overlays to allow force AC coupling, or specific roots
 * Fitting delay with min/max constraints
 * fitting with given relative degree (#zeros - #poles) or degree constraints
 * Multi-trial Order reduction
 * direct foton string format output


Documentation
-------------

 * [Sphinx docs ](https://lee-mcculler.docs.ligo.org/iirrational) (still only covers v1)
 * [V2 Command line documentation](https://git.ligo.org/lee-mcculler/iirrational/blob/master/IIRrational/v2/autogen_docstr.py)

 The command line program is named "IIRrationalV2fit" and provides the help above. It is a
 command line frontend to the function IIRrational.v2.data2filter. The frontend takes data from many 
 formats (.csv, .mat, .h5, .yaml, .json) and writes the fits, data and configuration to those same 
 formats.

 The output format/schema is intended to be an exchange format. If you post logs of your fits in the 
 output file format, they can be included in the test-suite. 

How to get:
-----------

This software repository holds most up-to-date code. Clone or download and install using pip. Note 
that the PyPI version is currently out of date.

> pip install IIRrational-version.tar.gz[all]

With the command line interface, it is also possible to run it from isolated installs, without 
requiring a python environment. 

 * [linux/win/osx Builds](https://git.ligo.org/lee-mcculler/iirrational-pkg/wikis/home)

Command Line Example
--------------------
 * [Oplev Plant Data](https://git.ligo.org/lee-mcculler/iirrational/raw/2.0.0/IIRrational_test_data/data/OplevPlant.mat)

```bash
> IIRrationalV2fit --mode printdata OplevPlant.mat fit.mat
```
```
ap:
  ex:
    ff: [(6401,) float64 array]
    plant: [(6401,) complex128 array]
  ey:
    ff: [(6401,) float64 array]
    plant: [(6401,) complex128 array]
  ix:
    ff: [(6401,) float64 array]
    plant: [(6401,) complex128 array]
  iy:
    ff: [(6401,) float64 array]
    plant: [(6401,) complex128 array]
```

Ok, Note that this data does NOT contain any SNR data. This is too bad as it really helps the fit to know where SNR is dropping. 
V2 now has an SNR estimator, which it will use by default if SNR is missing from the data.

To access the data in this file, set the `--data_group` to be `ap.ex`, `ap.ey` and so on. The Xfer function is complex and located 
in the `plant` field, and the frequency in the `ff` field for this data. Use the -Xc and -F (in the data: option group under --help output).

give it a try with
```bash
IIRrationalV2fit OplevPlant.mat fit.mat -Xc plant -F ff --data_group ap.ey --choose shell
```
Which will drop into a shell, allowing you to choose the final fit order and plot across the fits.
Once you exit the shell, this will output 
```
-L, --LIGO_foton=Sf output:
%%%
ZPK([
  -0.9812606552333539 + 0.13911398292307012*i; -0.9812606552333539 - 0.13911398292307012*i;
  -0.3693409490096515 + 2.0399565117236182*i; -0.3693409490096515 - 2.0399565117236182*i;
  -0.6390708712150615 + 6.121502130923322*i; -0.6390708712150615 - 6.121502130923322*i;
  3.2143953402691556 + 3.4127941868353635*i; 3.2143953402691556 - 3.4127941868353635*i;
  -0.5757175459634672;
],[
  -0.3588464583932162 + 0.3138212759268945*i; -0.3588464583932162 - 0.3138212759268945*i;
  -0.14745143728603557 + 1.5972037363406315*i; -0.14745143728603557 - 1.5972037363406315*i;
  -0.10816738941150655 + 2.6993788535912477*i; -0.10816738941150655 - 2.6993788535912477*i;
  -0.06787561811961029 + 1.4269943916018952*i; -0.06787561811961029 - 1.4269943916018952*i;
  -0.0195996981148364 + 0.5331862023977672*i; -0.0195996981148364 - 0.5331862023977672*i;
], -5.888951382123244e-06, "f")
%%%
```

Note that some of the other oplev plants in this file do not fit as well as this one... 
This could be remedied with some of the additional options, or with an estimate of the SNR.





